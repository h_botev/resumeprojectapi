<?php
namespace Docs\MainBundle\Exception;

/**
 * Interface that indicates whether an exception is human readable or not
 * @author hbotev
 *
 */
interface HumanReadableInterface
{
}
